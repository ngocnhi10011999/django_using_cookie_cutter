from django.conf import settings

from my_task.home.models import Category


def settings_context(_request):
    categories = Category.objects.all()
    """Settings available by default to the templates context."""
    # Note: we intentionally do NOT expose the entire settings
    # to prevent accidental leaking of sensitive information
    return {"DEBUG": settings.DEBUG, "categories": categories}
