from django.apps import AppConfig


class HomeApiConfig(AppConfig):
    name = 'my_task.home_api'
