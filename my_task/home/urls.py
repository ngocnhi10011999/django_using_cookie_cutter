from django.urls import path, include
from rest_framework import routers
from rest_framework_simplejwt import views as jwt_views
from . import views as v
from .api import views as va

router_ = routers.DefaultRouter()
app_name = "home"

user_list = va.UserViewSet.as_view({
    'get': 'list'
})
user_detail = va.UserViewSet.as_view({
    'get': 'retrieve'
})

product_list = va.ProductViewSet.as_view({
    'get': 'list',
    'post': 'create'
})
product_detail = va.ProductViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy'
})

categories_list = va.CategoryViewSet.as_view({
    'get': 'list',
    'post': 'create'
})
category_detail = va.CategoryViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy'
})

product_item_list = va.ProductItemViewSet.as_view({
    'get': 'list',
    'post': 'create'
})

product_item_detail = va.ProductItemViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy'
})

order_list = va.OrderViewSet.as_view({
    'get': 'list',
    'post': 'create'
})

order_detail = va.OrderViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy'
})

transaction_detail = va.TransactionViewSet.as_view({'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'})

transaction_list = va.TransactionViewSet.as_view({'get': 'list', 'post': 'create'})

urlpatterns = [
    path("", v.home, name="home"),
    path("search_product/", v.search_product, name='search_product'),
    path("product_detail/<slug:slug>/", v.detail, name="detail"),
    path("category/<slug:slug>/", v.category_detail, name="category"),
    path("add_to_cart/<slug:slug>", v.add_to_cart, name="add_to_cart"),
    path("delete_product/<int:id_>", v.delete_product, name="delete_product"),
    path("change_quantity/<int:id_>", v.change_quantity, name="change_quantity"),
    path("carts/", v.carts, name="carts"),
    path("check_out/", v.get_form_check_out, name="check_out"),
    path("invoice/", v.invoice, name="invoice"),
    path("invoice_detail/<int:id_trans>/", v.show_invoice, name="invoice_detail"),
]
urlpatterns += [
    path('api/token/', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
    path('hello/', v.HelloView.as_view(), name='hello'),

    path('root/', va.api_root),
    path('api/', include(router_.urls)),
    path('category_list/', categories_list, name='api_category_list'),
    path('category_detail/<int:pk>/', category_detail, name='api_category_detail'),
    path('product_list/', product_list, name='api_product_list'),
    path('product_detail_api/<int:pk>/', product_detail, name='api_product_detail'),
    path('user_list/', user_list, name='api_user_list'),
    path('user_detail/<int:pk>/', user_detail, name='api_user_detail'),
    # path('product_detail/<int:pk>', product_detail, name='api_product_detail'),
    path('product_item_list/', product_item_list, name='api_product_item_list'),
    path('product_item_detail/<int:pk>', product_item_detail, name='api_product_item_detail'),
    path('order_list/', order_list, name='api_order_list'),
    path('order_detail/<int:pk>', order_detail, name='api_order_detail'),
    path('transaction_list/', transaction_list, name='api_transaction_list'),
    path('transaction_detail/<int:pk>', transaction_detail, name='api_transaction_detail'),
]
