import json

from django.core.management.base import BaseCommand, CommandError
from ...models import Product, Category


class Command(BaseCommand):
    help = 'Create Product'

    def handle(self, *args, **options):
        with open('json_data/products.json', 'r') as prs:
            data = json.load(prs)
            for dt in data:
                try:
                    category = Category.objects.get(pk=dt['category'])
                    products = Product.objects.filter(name=dt['name']).update(
                        category=category, title=dt['title'], image=dt['image'],
                        price=dt['price'], in_stock=dt['in_stock'], slug=dt['slug'])
                    # product.opened = False
                    # print(products)
                    if products != 0:
                        self.stdout.write(self.style.SUCCESS('Update successfully!'))
                    else:
                        product = Product.objects.create(
                            name=dt['name'], category=category, title=dt['title'], image=dt['image'],
                            price=dt['price'], in_stock=dt['in_stock'], slug=dt['slug'])
                        self.stdout.write(self.style.SUCCESS('Successfully product "%s"' % product.name))
                except Product.DoesNotExist:
                    raise CommandError('Product does not exist')



