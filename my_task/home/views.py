from django.contrib.auth.decorators import login_required
from django.contrib import messages
# from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect, render

from .models import Category, Order, Product, ProductItem, Transaction

# Create your views here.
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated


class HelloView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self):
        content = {'message': 'Hello, World!'}
        return Response(content)


def home(request):
    products = Product.objects.all()
    # # print('quality', quality)
    context = {
        "products": products
    }
    return render(request, "home/index.html", context)


def search_product(request):
    text_search = request.GET.get('search')
    products = Product.objects.filter(name__contains=text_search.strip())
    if text_search == '':
        messages.error(request, 'The search is not empty')
        return redirect("home:home")
    # print('search_text', products)
    context = {
        'products': products
    }
    return render(request, "home/search_result.html", context)


def detail(request, slug):
    product_detail = get_object_or_404(Product, slug=slug)
    context = {
        "p": product_detail,
    }
    return render(request, "home/product_detail.html", context)


def category_detail(request, slug):
    category = get_object_or_404(Category, slug=slug)
    products = category.products.all()
    context = {"products": products}
    return render(request, "home/category.html", context)


def add_to_cart(request, slug):
    product = get_object_or_404(Product, slug=slug)
    if request.user.is_authenticated:
        order, created = Order.objects.get_or_create(user=request.user, completed=False)
        order.save()
        item, create = ProductItem.objects.get_or_create(product=product, order=order)
        # print('created', create)
        if create:
            item.quantity = 1
            item.save()
        else:
            item.quantity += 1
            item.save()
        return redirect("home:detail", slug=product.slug)
        # return HttpResponseRedirect("")

    else:
        return redirect("send_mail_user:login")


@login_required(login_url='send_mail_user:login')
def carts(request):
    order = Order.objects.filter(user=request.user, completed=False).first()
    if order is not None:
        pass
    else:
        return render(request, "home/cart_404.html", context=None)
    product_items = order.product_items.all()
    sum_price = Order.payment(user=request.user)
    context = {"product_items": product_items, "sum_price": sum_price}
    return render(request, "home/carts.html", context)


def delete_product(request, id_):
    item = ProductItem.objects.get(pk=id_)
    if request.user.has_perm("home.delete_productitem"):
        item.delete()
    return redirect("home:carts")


@login_required(login_url='send_mail_user:login')
def change_quantity(request, id_):
    item = ProductItem.objects.get(pk=id_)
    quantity = request.POST.get('quantity')
    if request.user.has_perm("home.change_productitem"):
        item.quantity = quantity
        item.save()
    return redirect("home:carts")


@login_required(login_url='send_mail_user:login')
def get_form_check_out(request):
    return render(request, "home/check_out.html", context=None)


@login_required(login_url='send_mail_user:login')
def invoice(request):
    try:
        order = Order.objects.get(user=request.user, completed=False)
        sum_price = Order.payment(user=request.user)
        transaction, created = Transaction.objects.get_or_create(
            user=request.user, order=order, payment=sum_price
        )
        transaction.save()
        order.completed = True
        order.save()

        for item in order.product_items.all():
            product = Product.objects.get(product_items=item)
            product.in_stock -= item.quantity
            product.save()

        context = {
            'transaction': transaction,
            'sum_price': sum_price,
        }
        return render(request, "home/invoice.html", context=context)
    except Order.DoesNotExist:
        transactions = Transaction.objects.filter(user=request.user)
        return render(request, "home/list_invoice.html", context={'transactions': transactions})


@login_required(login_url='send_mail_user:login')
def show_invoice(request, id_trans):
    transaction = get_object_or_404(Transaction, pk=id_trans)

    context = {
        'transaction': transaction
    }
    return render(request, "home/invoice.html", context=context)
