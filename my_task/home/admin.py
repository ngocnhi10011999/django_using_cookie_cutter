from django.contrib import admin

from . import models as md


admin.site.register(md.Transaction)
admin.site.register(md.Product)
admin.site.register(md.Order)
admin.site.register(md.ProductItem)
admin.site.register(md.Category)
