from django.contrib.auth import get_user_model
from rest_framework.relations import HyperlinkedIdentityField

from ..models import Category, Product, ProductItem, Order, Transaction
from rest_framework import serializers

User = get_user_model()


# class UserSerializer(serializers.ModelSerializer):
#     order = serializers.PrimaryKeyRelatedField(many=True, queryset=Order.objects.all())
#     transactions = serializers.StringRelatedField(many=True,  read_only=True)
#
#     class Meta:
#         model = User
#         fields = ["username", "email", "name", "url", 'order', 'transactions']
#
#         extra_kwargs = {
#             "url": {"view_name": "api:user-detail", "lookup_field": "username"}
#         }


class UserSerializer(serializers.HyperlinkedModelSerializer):
    url = HyperlinkedIdentityField(view_name='home:api_user_detail')
    transactions = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    orders = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        model = User
        fields = ['url', 'id', 'username', 'orders', 'transactions']


class CategorySerializer(serializers.HyperlinkedModelSerializer):
    url = HyperlinkedIdentityField(view_name='home:api_category_detail')
    products = serializers.StringRelatedField(many=True)

    class Meta:
        model = Category
        fields = ['url', 'name', 'slug', 'products']


class ProductSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='home:api_product_detail')
    # category = serializers.HyperlinkedIdentityField(view_name='home:api_category_detail')
    # product_items = serializers.StringRelatedField(many=True, read_only=True)

    class Meta:
        model = Product
        fields = ['url', 'category', 'name', 'title', 'image', 'price', 'in_stock', 'slug']

    def create(self, validated_data):
        """
        Create and return a new `Snippet` instance, given the validated data.
        """
        return Product.objects.create(**validated_data)


class ProductItemSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='home:api_product_item_detail')

    class Meta:
        model = ProductItem
        fields = ['url', 'id', 'order', 'product', 'quantity']

    def create(self, validated_data):

        return ProductItem.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `Snippet` instance, given the validated data.
        """
        instance.order = validated_data.get('order', instance.order)
        instance.product = validated_data.get('product', instance.product)
        instance.quantity = validated_data.get('quantity', instance.quantity)
        instance.save()
        return instance


class OrderSerializer(serializers.ModelSerializer):
    url = HyperlinkedIdentityField(view_name="home:api_order_detail")
    transactions = serializers.StringRelatedField(many=True, read_only=True)
    user = UserSerializer()
    order_items = serializers.SerializerMethodField()

    class Meta:
        model = Order
        fields = ['url', 'id', 'user', 'completed', 'transactions', "order_items"]

    def create(self, validated_data):
        """
        Create and return a new `Snippet` instance, given the validated data.
        """
        return Order.objects.create(**validated_data)

    def get_order_items(self, obj):
        return ProductItemSerializer(obj.product_items.all(), many=True, context=self.context).data


class TransactionSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='home:api_transaction_detail')
    user = serializers.ReadOnlyField(source='user.username')
    # order = serializers.SerializerMethodField()
    # order = HyperlinkedIdentityField(view_name='home:api_order_detail')

    class Meta:
        model = Transaction
        fields = ['url', 'user', 'order', 'payment', 'success', 'cancel_order']

        # def get_order(self, obj):
        #     return OrderSerializer(obj.product_items.all(), many=True, context=self.context).data
