from django.contrib.auth import get_user_model

from rest_framework.reverse import reverse
from rest_framework import viewsets
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .serializers import (UserSerializer, CategorySerializer, ProductSerializer, ProductItemSerializer, OrderSerializer,
                          TransactionSerializer)
from ..models import Category, Product, ProductItem, Order, Transaction

User = get_user_model()


class UserViewSet(viewsets.ReadOnlyModelViewSet):
    """
    This viewset automatically provides `list` and `detail` actions.
    """

    def throttled(self, request, wait):
        return super().throttled(request, wait)

    def retrieve(self, request, *args, **kwargs):
        return super().retrieve(request, *args, **kwargs)

    queryset = User.objects.all()
    serializer_class = UserSerializer


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    # permission_classes = [permissions.IsAuthenticatedOrReadOnly,
    #                       IsOwnerOrReadOnly]


class ProductItemViewSet(viewsets.ModelViewSet):
    queryset = ProductItem.objects.all()
    serializer_class = ProductItemSerializer

    def perform_create(self, serializer):
        order, created = Order.objects.get_or_create(user=self.request.user, completed=False)
        order.save()
        serializer.save(order=order)


class OrderViewSet(viewsets.ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer


class TransactionViewSet(viewsets.ModelViewSet):
    queryset = Transaction.objects.all()
    serializer_class = TransactionSerializer

    def perform_create(self, serializer):
        order = Order.objects.get(user=self.request.user, completed=False)
        sum_price = Order.payment(user=self.request.user)
        serializer.save(user=self.request.user, order=order, payment=sum_price)
        order.completed = True
        order.save()


@api_view(['GET'])
def api_root(request, format=None):
    return Response({
        'product-list': reverse('home:api_product_list', request=request, format=format),
        'transaction-list': reverse('home:api_transaction_list', request=request, format=format),

        'users-list': reverse('home:api_user_list', request=request, format=format)
    })
