from django.db import models
from django.template.defaultfilters import slugify

# Create your models here.
from config.settings.base import AUTH_USER_MODEL


# Create your models here.


class Category(models.Model):
    name = models.CharField(max_length=200, unique=True)
    slug = models.SlugField(max_length=200, blank=True, unique=True)

    # products = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.slug

    def save(self, *args, **kwargs):
        if not self.pk:
            self.slug = slugify(self.name)
        super(Category, self).save(*args, **kwargs)


class Product(models.Model):
    category = models.ForeignKey(Category, related_name='products', on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    title = models.CharField(max_length=200)
    image = models.ImageField(blank=True, null=True)  # imageField
    price = models.DecimalField(max_digits=10, decimal_places=0)
    in_stock = models.IntegerField(default=0)  # stock
    slug = models.SlugField(max_length=200)

    def __str__(self):
        return "{0} - category: {1}".format(self.name, self.category)


class Order(models.Model):
    user = models.ForeignKey(
        AUTH_USER_MODEL, related_name='orders', on_delete=models.CASCADE, blank=True, null=True
    )
    date_ordered = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    completed = models.BooleanField(default=False)

    # transaction = models.TextField(blank=True, null=True)

    def __str__(self):
        return "{0} {1}".format(
            self.user,
            self.date_ordered.strftime("%m/%d/%Y, %H:%M"),
        )

    @classmethod
    def payment(cls, user):
        order = cls.objects.get(user=user, completed=False)
        sum_price = 0
        product_items = order.product_items.all()
        for item in product_items:
            sum_price += item.line_total()
        return sum_price


class ProductItem(models.Model):
    product = models.ForeignKey(Product, related_name='product_items', on_delete=models.CASCADE)
    order = models.ForeignKey(Order, related_name='product_items', on_delete=models.CASCADE, blank=True, null=True)  # change orders to product item
    quantity = models.IntegerField(default=0, blank=True, null=True)
    date_added = models.DateTimeField(auto_now_add=True, blank=True, null=True)

    # orders = models.TextField(blank=True, null=True)

    def __str__(self):
        return "{0} - quantity: {1}".format(self.product.name, self.quantity)

    def line_total(self):
        return self.quantity * self.product.price


class Transaction(models.Model):
    user = models.ForeignKey(AUTH_USER_MODEL, related_name='transactions', on_delete=models.CASCADE)
    date = models.DateField(auto_now=True, blank=True, null=True)
    order = models.ForeignKey(Order, related_name='transactions', on_delete=models.CASCADE, blank=True, null=True)
    payment = models.DecimalField(
        max_digits=10, decimal_places=0, blank=True, null=True
    )
    success = models.BooleanField(default=True)
    cancel_order = models.BooleanField(default=False)

    def __str__(self):
        return "{0} Transaction on {1} - {2}".format(self.user, self.date, self.pk)
