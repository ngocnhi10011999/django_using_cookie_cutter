from django.apps import AppConfig


class HomeConfig(AppConfig):
    name = "my_task.home"
