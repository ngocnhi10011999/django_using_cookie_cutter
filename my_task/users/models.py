from django.contrib.auth.models import AbstractUser
from django.db.models import CharField, Sum
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from ..home.models import ProductItem


class User(AbstractUser):
    """Default user for My task."""

    #: First and last name do not cover name patterns around the globe
    name = CharField(_("Name of User"), blank=True, max_length=255)

    def calculator_quality_order(self):
        orders = self.orders.filter(completed=False)
        product_item = ProductItem.objects.filter(order__in=orders)
        quantity = product_item.aggregate(Sum("quantity"))[
            "quantity__sum"
        ]

        return quantity or 0

    def get_absolute_url(self):
        """Get url for user's detail view.

        Returns:
            str: URL for user detail.

        """
        return reverse("users:detail", kwargs={"username": self.username})

    def __str__(self):
        return self.username
