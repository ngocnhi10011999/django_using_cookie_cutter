from django.urls import path

from .views import LoginView, LogoutView, RegistrationView, VerificationView

app_name = "send_mail_user"
urlpatterns = [
    path("register/", RegistrationView.as_view(), name="register"),
    path("login/", LoginView.as_view(), name="login"),
    path("logout/", LogoutView.as_view(), name="logout"),
    path('activate/<uidb64>/<token>',
         VerificationView.as_view(), name='activate'),
]
