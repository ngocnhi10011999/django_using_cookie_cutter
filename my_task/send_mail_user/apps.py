from django.apps import AppConfig


class SendMailUserConfig(AppConfig):
    name = "my_task.send_mail_user"
