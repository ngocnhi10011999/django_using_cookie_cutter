from django.contrib import auth, messages
from django.contrib.auth.models import Permission
from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import EmailMessage
from django.shortcuts import redirect, render
from django.urls import reverse
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.contrib.contenttypes.models import ContentType
from django.views import View

from my_task.home.models import ProductItem
from my_task.send_mail_user.utils import account_activation_token
from my_task.users.models import User

# from .utils import account_activation_token

# Create your views here.


class RegistrationView(View):
    def get(self, request):
        return render(request, "send_mail_user/register.html")

    def post(self, request):
        # GET USER DATA
        # VALIDATE
        # create a user account

        username = request.POST["username"]
        email = request.POST["email"]
        password = request.POST["password"]
        retype_password = request.POST["retypepassword"]
        if password == retype_password:
            pass
        else:
            messages.error(request, "Password incorrect")

        context = {"fieldValues": request.POST}

        if not User.objects.filter(username=username).exists():
            if not User.objects.filter(email=email).exists():
                if len(password) < 6:
                    messages.error(request, "Password too short")
                    return render(request, "send_mail_user/register.html", context)

                content_type = ContentType.objects.get_for_model(ProductItem)
                change_productitem = Permission.objects.get(
                    codename='change_productitem',
                    content_type=content_type,
                )
                delete_productitem = Permission.objects.get(
                    codename='delete_productitem',
                    content_type=content_type,
                )
                user = User.objects.create_user(username=username, email=email)
                user.set_password(password)
                user.user_permissions.add(change_productitem, delete_productitem)
                user.is_active = False
                user.save()
                current_site = get_current_site(request)
                email_body = {
                    'user': user,
                    'domain': current_site.domain,
                    'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                    'token': account_activation_token.make_token(user),
                }

                link = reverse('send_mail_user:activate', kwargs={
                               'uidb64': email_body['uid'], 'token': email_body['token']})

                email_subject = "Activate your account"

                activate_url = 'http://127.0.0.1:8000'+link
                # print('url', current_site.domain)
                email = EmailMessage(
                    email_subject,
                    'Hi '+user.username + ', Please the link below to activate your account \n'+activate_url,
                    'noreply@semycolon.com',
                    [email],
                )
                email.send(fail_silently=False)
                messages.success(request, "Account successfully created")
                return render(request, "send_mail_user/register.html")

        return render(request, "send_mail_user/register.html")


class VerificationView(View):
    def get(self, request, uidb64, token):
        try:
            id = force_text(urlsafe_base64_decode(uidb64))
            user = User.objects.get(pk=id)

            if not account_activation_token.check_token(user, token):
                return redirect('send_mail_user:login'+'?message='+'User already activated')

            if user.is_active:
                return redirect('send_mail_user:login')
            user.is_active = True
            user.save()

            messages.success(request, 'Account activated successfully')
            return redirect('send_mail_user:login')

        except Exception:
            pass

        return redirect('send_mail_user:login')


class LoginView(View):
    def get(self, request):
        return render(request, "send_mail_user/login.html", context=None)

    def post(self, request):
        username = request.POST["username"]
        password = request.POST["password"]

        if username and password:
            user = auth.authenticate(username=username, password=password)

            if user:
                if user.is_active:
                    auth.login(request, user)
                    messages.success(
                        request,
                        "Welcome, " + request.user.username + " you are now logged in",
                    )
                    return redirect("home:home")
                messages.error(request, "Account is not active,please check your email")
                return render(request, "send_mail_user/login.html")
            messages.error(request, "Invalid credentials,try again")
            return render(request, "send_mail_user/login.html")

        messages.error(request, "Please fill all fields")
        return render(request, "send_mail_user/login.html")


class LogoutView(View):
    def get(self, request):
        auth.logout(request)
        messages.success(request, "You have been logged out")
        return redirect("home:home")
